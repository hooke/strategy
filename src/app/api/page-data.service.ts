import {Injectable} from '@angular/core';

const staticPath = './assets/';

const pageData = {
    'application': {
        'slides': [{
            'img': staticPath + '720-300.png',
        }, {
            'img': staticPath + '720-300.png',
        }, {
            'img': staticPath + '720-300.png',
        }],
        'list': [{
            'logo': staticPath + '120-120.png',
            'name': '火币',
            'desc': '比特币区块链学习问答平台',
            'score': '5.0',
            'count': 5000,
            'type': 'exchange',
            'androidHref': 'http://gdown.baidu.com/data/wisegame/e2b152dd1bd7eedc/yingyongbao_7252130.apk',
            'iosHref': 'http://gdown.baidu.com/data/wisegame/e2b152dd1bd7eedc/yingyongbao_7252130.apk'
        }, {
            'logo': staticPath + '120-120.png',
            'name': '币响',
            'desc': '比特币区块链学习问答平台',
            'score': '4.0',
            'count': 8000,
            'type': 'news',
            'androidHref': 'http://gdown.baidu.com/data/wisegame/e2b152dd1bd7eedc/yingyongbao_7252130.apk',
            'iosHref': 'http://gdown.baidu.com/data/wisegame/e2b152dd1bd7eedc/yingyongbao_7252130.apk'
        }, {
            'logo': staticPath + '120-120.png',
            'name': '情报',
            'desc': '比特币区块链学习问答平台',
            'score': '3.0',
            'count': 1000,
            'type': 'wallet',
            'androidHref': 'http://gdown.baidu.com/data/wisegame/e2b152dd1bd7eedc/yingyongbao_7252130.apk',
            'iosHref': 'http://gdown.baidu.com/data/wisegame/e2b152dd1bd7eedc/yingyongbao_7252130.apk'
        }, {
            'logo': staticPath + '120-120.png',
            'name': '币世界',
            'desc': '比特币区块链学习问答平台',
            'score': '4.0',
            'count': 9000,
            'type': 'chat',
            'androidHref': 'http://gdown.baidu.com/data/wisegame/e2b152dd1bd7eedc/yingyongbao_7252130.apk',
            'iosHref': 'http://gdown.baidu.com/data/wisegame/e2b152dd1bd7eedc/yingyongbao_7252130.apk'
        }]
    },
    'exchange': {
        'list': [{
            'logo': staticPath + '140-60.png',
            'name': '火币',
            'sum': '¥50亿',
            'tradeType': '现货'
        }, {
            'logo': staticPath + '140-60.png',
            'name': '火币1',
            'sum': '¥5000亿',
            'tradeType': '现货'
        }, {
            'logo': staticPath + '140-60.png',
            'name': '火币2',
            'sum': '¥5亿',
            'tradeType': '现货'
        }, {
            'logo': staticPath + '140-60.png',
            'name': '火币3',
            'sum': '¥500亿',
            'tradeType': '现货'
        }]
    },
    'articleList': {
        'list': [{
            'img': staticPath + '720-280.png',
            'href': 'http://www.baidu.com'
        }, {
            'img': staticPath + '720-280.png',
            'href': 'http://www.baidu.com'
        }]
    },
    'wechatNumber': {
        'list': [{
            'logo': staticPath + '120-120.png',
            'name': '每日必读',
            'reason': '比特币区块链学习问答平台, 比特币区块链学习问答平台',
            'funNum': 5000,
            'ID': 'meiribidu1'
        }, {
            'logo': staticPath + '120-120.png',
            'name': '每日必读',
            'reason': '比特币区块链学习问答平台, 比特币区块链学习问答平台',
            'funNum': 5000,
            'ID': 'meiribidu2'
        }, {
            'logo': staticPath + '120-120.png',
            'name': '每日必读',
            'reason': '比特币区块链学习问答平台, 比特币区块链学习问答平台',
            'funNum': 5000,
            'ID': 'meiribidu3'
        }, {
            'logo': staticPath + '120-120.png',
            'name': '每日必读',
            'reason': '比特币区块链学习问答平台, 比特币区块链学习问答平台',
            'funNum': 5000,
            'ID': 'meiribidu4'
        }]
    }
};

@Injectable({
    providedIn: 'root'
})
export class PageDataService {
    public pageData = pageData;

    constructor() {
    }
}
