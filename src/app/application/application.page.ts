import {Component, OnInit} from '@angular/core';
import {PageDataService} from '../api/page-data.service';

@Component({
    selector: 'app-application',
    templateUrl: './application.page.html',
    styleUrls: ['./application.page.scss'],
})
export class ApplicationPage implements OnInit {
    public slideOpts = {
        effect: 'slide',
    };

    activeIndex = 'all';
    public pageData: any;

    get list(): any[] {
        const list = this.pageData.list;
        if (list && list.length) {
            return list.filter((item) => {
                if (this.activeIndex === 'all' || this.activeIndex === item.type) {
                    return true;
                }
                return false;
            });
        }
        return [];
    }

    public tabList = [{
        'name': '全部分类',
        'type': 'all',
        'active': true
    }, {
        'name': '交易所',
        'type': 'exchange',
        'active': false
    }, {
        'name': '资讯知识',
        'type': 'news',
        'active': false
    }, {
        'name': '钱包',
        'type': 'wallet',
        'active': false
    }, {
        'name': '社群聊天',
        'type': 'chat',
        'active': false
    }];

    constructor(private pageDataService: PageDataService) {
        this.pageData = pageDataService.pageData.application;
        console.log('this.pageData', this.pageData);
    }

    ngOnInit() {
    }

    segmentChanged($event) {
        console.log('$event.detail.value', $event.detail.value);
        this.activeIndex = $event.detail.value;
    }

    onTabButtonClickHandler(item) {
        this.tabList.forEach((tab) => {
            if (tab.type === item.type) {
                this.activeIndex = tab.type;
                tab.active = true;
            } else {
                tab.active = false;
            }
        });
    }
}


