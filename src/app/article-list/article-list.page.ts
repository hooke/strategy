import {Component, OnInit} from '@angular/core';
import {PageDataService} from '../api/page-data.service';

@Component({
    selector: 'app-article-list',
    templateUrl: './article-list.page.html',
    styleUrls: ['./article-list.page.scss'],
})
export class ArticleListPage implements OnInit {
    public pageData: any;
    constructor(public pageDataService: PageDataService) {
        this.pageData = this.pageDataService.pageData.articleList;
    }

    ngOnInit() {
    }

}
