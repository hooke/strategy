import {Component, OnInit} from '@angular/core';
import {PageDataService} from '../api/page-data.service';

@Component({
    selector: 'app-exchange',
    templateUrl: './exchange.page.html',
    styleUrls: ['./exchange.page.scss'],
})
export class ExchangePage implements OnInit {

    pageData: any;
    constructor(private pageDataService: PageDataService) {
        this.pageData = this.pageDataService.pageData.exchange;
    }

    ngOnInit() {
    }

}
