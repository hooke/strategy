import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {TabsPageRoutingModule} from './tabs.router.module';

import {TabsPage} from './tabs.page';

import {ApplicationModule} from '../application/application.module';
import {ExchangeModule} from '../exchange/exchange.module';
import {ArticleListModule} from '../article-list/article-list.module';
import {WechatNumberModule} from '../wechat-number/wechat-number.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        TabsPageRoutingModule,
        ApplicationModule,
        ArticleListModule,
        ExchangeModule,
        WechatNumberModule,
    ],
    declarations: [TabsPage]
})
export class TabsPageModule {
}
