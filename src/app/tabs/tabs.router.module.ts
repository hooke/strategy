import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {TabsPage} from './tabs.page';

import {ApplicationPage} from '../application/application.page';
import {ArticleListPage} from '../article-list/article-list.page';
import {ExchangePage} from '../exchange/exchange.page';
import {WechatNumberPage} from '../wechat-number/wechat-number.page';

const routes: Routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            {
                path: 'application',
                outlet: 'application',
                component: ApplicationPage
            }, {
                path: 'article-list',
                outlet: 'article-list',
                component: ArticleListPage
            }, {
                path: 'exchange',
                outlet: 'exchange',
                component: ExchangePage
            }, {
                path: 'wechat-number',
                outlet: 'wechat-number',
                component: WechatNumberPage
            }
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/(application:application)',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsPageRoutingModule {
}
