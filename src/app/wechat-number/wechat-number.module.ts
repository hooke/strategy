import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';
import {ClipboardModule} from 'ngx-clipboard';

import {WechatNumberPage} from './wechat-number.page';

const routes: Routes = [
    {
        path: '',
        component: WechatNumberPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ClipboardModule
    ],
    declarations: [WechatNumberPage]
})
export class WechatNumberModule {
}
