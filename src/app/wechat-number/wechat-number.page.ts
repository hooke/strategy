import {Component, OnInit} from '@angular/core';
import {PageDataService} from '../api/page-data.service';

interface IPageData {
    list: {
        'logo': string;
        'name': string;
        'reason': string;
        'funNum': number;
        'ID': string;
        'hasCopy'?: boolean;
    }[];
}

@Component({
    selector: 'app-wechat-number',
    templateUrl: './wechat-number.page.html',
    styleUrls: ['./wechat-number.page.scss'],
})
export class WechatNumberPage implements OnInit {
    public pageData: IPageData;

    constructor(public pageDataService: PageDataService) {
        this.pageData = this.pageDataService.pageData.wechatNumber;
    }

    ngOnInit() {
    }

    OnCopySuccessHandler($event, item) {
        this.pageData.list.forEach((data) => {
           if (data.ID !== item.ID) {
               data.hasCopy = false;
           } else {
               item.hasCopy = true;
           }
        });
    }
}
